import {
  Avatar,
  Button,
  Col,
  Form,
  Input,
  message,
  Checkbox,
  Switch,
} from "antd";
import axios from "axios";
import { useForm } from "rc-field-form";
import React, { useEffect, useState } from "react";
import CustomerService from "../../services/auth/CustomerService";
import styles from "./Info.module.scss";

export default function Info() {
  const emailUser = localStorage.getItem("emailUser");
  const [user, setUser] = useState({});
  const [visibleSwitch,setVisibleSwitch] = useState(false);
  const [componentDisabled, setComponentDisabled] = useState(true);

  const onFormLayoutChange = ({ disabled }) => {
    setComponentDisabled(disabled);
  };

  const onFinish = async (values) => {
    console.log(values);
    let res = await CustomerService.updateCustomer(values.id, values);
    message.success("Cập nhật thông tin thành công !");
    loadDetail();
  };

  const handleChange = (e) => {
    if (e) {
      setVisibleSwitch(true);
      setComponentDisabled(false);
    } else {
      setVisibleSwitch(false);
      setComponentDisabled(true);
    }
  };

  const loadDetail = async () => {
    let res = await CustomerService.getByEmail(emailUser);
    setUser(res.data);
    console.log(res.data);
  };

  useEffect(() => {
    if (emailUser) {
      loadDetail();
    }
  }, []);

  return (
    <Col
      lg={{ span: 12, offset: 6 }}
      md={{ span: 12, offset: 6 }}
      xs={{ span: 22, offset: 1 }}
      sm={{ span: 22, offset: 1 }}
    >
      <div className={styles.container}>
        <div className={styles.background}>
          <Avatar
            src={
              "https://lh3.googleusercontent.com/a/AItbvmmKSQOLDvZFY8v8IlIl6_dwjTT-hfZK7A7zlMJd=s96-c"
            }
            size={64}
            className={styles.avatar}
          ></Avatar>
          <div className={styles.name}>
            <h4>
              {user.firstName} {user.lastName}
            </h4>
          </div>
        </div>
        <div className={styles.info}>
        <Switch onChange={handleChange} style={{marginLeft:"5rem"}}/> {visibleSwitch ? <span className={styles.switch}>Chỉnh sửa</span> : <span className={styles.switch}>Xem thông tin</span>}

        <Col lg={{span:22,offset:1}}
        sm={{span:22,offset:1}}
        md={{span:22,offset:1}}
        xs={{span:22,offset:1}}
        className={styles.form}
        > 
        <Form
            labelCol={{
              span: 4,
              offset: 2,
            }}
            wrapperCol={{
              span: 14,
            }}
            layout="horizontal"
            onValuesChange={onFormLayoutChange}
            disabled={componentDisabled}
          >
            <Form.Item label="Họ và tên đệm">
              <Input />
            </Form.Item>
            <Form.Item label="Tên">
              <Input />
            </Form.Item>
            <Form.Item label="Số điện thoại">
              <Input />
            </Form.Item>
            <Form.Item label="Địa chỉ">
              <Input />
            </Form.Item>
            <Form.Item label="Quốc gia">
              <Input />
            </Form.Item>
            <Form.Item label="Thành phố">
              <Input />
            </Form.Item>
          </Form>
          </Col> 
        </div>
      </div>
    </Col>
  );
}
