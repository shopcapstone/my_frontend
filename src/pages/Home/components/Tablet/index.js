import { Col } from "antd";
import React, { useEffect, useState } from "react";
import SliderPhone from "../Phone/SliderPhone";
import styles from "./Tablet.module.scss";

const Tablet = () => {
  const [product, setProduct] = useState([]);
  const loadProduct = async () => {
    try {
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    loadProduct();
  }, []);
  return (
    <>
      <Col
        lg={{ span: 16, offset: 4 }}
        md={{ span: 16, offset: 4 }}
        sm={{ span: 22, offset: 1 }}
        xs={{ span: 22, offset: 1 }}
        className={styles.container}
      > 
        <SliderPhone name="Máy tính bảng" data={product}></SliderPhone>
      </Col>
      
    </>
  );
};

export default Tablet;
