import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import CardProduct from "../../../../components/CardProduct";
import styles from "./SliderPhone.module.scss";
import { Empty } from "antd";

const SliderPhone = ({ data, name }) => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
    ],
  };
  return (
    <div>
      <div className={styles.header}>
            <div className={styles.heading}>{name}</div>
          </div>
      {data.length ? (
        <>
          <Slider {...settings}>
            {data?.map((product) => {
              return (
                <>
                  <div className={styles.container}>
                    <CardProduct product={product}></CardProduct>
                  </div>
                </>
              );
            })}
          </Slider>
        </>
      ) : (
        <Empty description={"Đang cập nhật . . ."} className={styles.empty}></Empty>
      )}
    </div>
  );
};

export default SliderPhone;
